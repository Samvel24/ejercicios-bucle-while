
package ejerciciosbuclewhile;
import java.util.Scanner;

public class Ejercicio 
{  
    public Ejercicio() {
        // Constructor
    }
    
    /*
    Método para leer una lista de números y calcular el producto acumulado de los primeros 7 
    múltiplos de 5. Al final imprimir la acumulación en cuestión.
    */
    public void ejercicioNumeroUno() {
        Scanner keyboard = new Scanner(System.in);
        int cont, num;
        long prod; // Podemos usar el tipo long para enteros muy grandes
	prod = 1;
	cont = 1;
        
        while(cont <= 7)
	{
            System.out.print("Dame un número: ");
            num = keyboard.nextInt();
            
            if(num % 5 == 0) // ¿Es multiplo de 5?
            {
		prod = prod * num;
		cont++;		
            }
	}
        
        System.out.println("El producto es: " + prod);
    }
    
    /*
    Método para leer una lista de calificaciones y calcular el promedio, el número de aprobados y 
    reprobados; el programa deberá terminar cuando se introduzca un número negativo.
    */
    public void ejercicioNumeroDos() {
        Scanner keyboard = new Scanner(System.in);
        float prom; // variable para guardar el promedio de calificaciones
	int acum; // variable para acumular la suma de las calificaciones
	int aprobados; // variable para contar aprobados
	int reprobados; // variable para contar reprobados
	int calif; // variable que guarda cada calificacion
	int totalcalif; // variable que guarda el numero total de calificaciones ingresadas
        
	acum = 0;
	aprobados = 0;
	reprobados = 0;
        // inicializamos la variable calif para poder entrar al bucle while en la primera iteración
        calif = 10;
	while(calif > 0) {  	
            System.out.print("Dame una calificacion: ");
            calif = keyboard.nextInt();
 
            if (calif > 0) {
                acum = acum + calif;
                
                if (calif <= 5) {
                    reprobados++;
                }
                else {
                    aprobados++;
                }
            }			
	}
        
	totalcalif = aprobados + reprobados;
	prom = acum / totalcalif;
	System.out.println("El promedio es: " + prom);
	System.out.println("El numero de aprobados es: " + aprobados);
	System.out.println("El numero de reprobados es: " + reprobados);
    }
    
    /*
    Leer una lista de números positivos y obtener el máximo y el mínimo, un número negativo 
    termina el ciclo. El negativo no deberá ser tomado en cuenta.
    */
    public void ejercicioNumeroTres() {
        Scanner keyboard = new Scanner(System.in);
        int num, maximo, minimo;
     
	System.out.print("Dame un numero entero: ");
	num = keyboard.nextInt();
	
        maximo = num; // inicialmente el número ingresado es el máximo y mínimo
	minimo = num;
	while (num >= 0) {
            if (num > maximo) {
                maximo = num;
            }
            if (num < minimo) {
                minimo = num;
            }
            
            System.out.print("Dame un numero entero: ");
            num = keyboard.nextInt();
	}
        
	if(maximo >= 0 && minimo >= 0) {
            System.out.println("El mayor es: " + maximo);
            System.out.println("El menor es: " + minimo);
	}
        else {
            System.out.println("No se ingresaron numeros positivos.");
        }
    }
    
    /*
    Imprimir en pantalla los términos de la serie de Fibonacci hasta que rebase un valor n 
    (entero y positivo) leído por el teclado.
    En esta serie los dos primeros números son 1, y el resto se obtiene sumando los dos anteriores.
    */
    public void ejercicioNumeroCuatro() {
        int acum, n1, n2, n, cont;
        Scanner keyboard = new Scanner(System.in);
	n1 = 1;
	n2 = 1;
	cont = 3;
        System.out.print("Dame el numero de terminos que quieres ver de la serie Fibonacci: ");
	n = keyboard.nextInt();
        
	if (n > 0) {
            System.out.println("La serie con " + n + " terminos es: ");
            
            if (n == 1) {
                System.out.println(n1);
            }
            else if (n == 2) {
                System.out.print(n1 + ", " + n2);
            }
            else if (n > 2){
                System.out.print(n1 + ", " + n2 + ", ");
                while (cont <= n) {
                    if(cont != n) {
                        acum = n1 + n2;
                        System.out.print(acum + ", ");
                        n1 = n2;
                        n2 = acum;
                        cont++;
                    }
                    else { // en el ultimo termino de la serie no colocamos la coma separadora
                        acum = n1 + n2;
                        System.out.print(acum);
                        n1 = n2;
                        n2 = acum;
                        cont++;
                    }
                }
            }                   
        }
        else {
            System.out.println("No se ingreso un numero de terminos valido");
        }
        
        System.out.println("");
    }
    
    /*
    Conjetura de ULAM: Comience con cualquier número entero positivo. Si es par divídalo entre 2 y 
    si es impar multiplique por 3 y aumente en 1. Calcular e imprimir los números enteros 
    positivos repitiendo el proceso hasta llegar a 1. Por ejemplo si empezamos en 5 la serie 
    debería ser: 5, 16, 8, 4, 2, 1.
    
    Esta conjetura afirma que si C(𝑛) es la función de Collatz definida por C(n)=n/2 si n es par 
    y por C(n)=3n+1 si n es impar, entonces tras un número finito de iteraciones de C(n) se llega 
    al valor 1 independientemente del valor entero positivo de partida.
    Ver: https://temat.es/articulo/2022-p65
    */
    public void ejercicioNumeroCinco() {
        System.out.println("Conjetura ULAM");
        int num;
        Scanner keyboard = new Scanner(System.in);
	System.out.print("Dame un numero: ");
	num = keyboard.nextInt();
        
	if(num > 0) {
		System.out.println(num);
		while (num != 1) {
                    if (num % 2 == 0) {
                        num = num / 2;
                        System.out.println(num);
                    }
                    else {
                        num = num * 3;
                        num++;
                        System.out.println(num);
                    }
		}
	}
        else {
            System.out.println("No se ingreso un numero poitivo.");
        }
    }
    
    /*
    Método para calcular las siguientes series:
    a) 1/1 + 1/2 + 1/3 + 1/4 +...+ 1/n
    b) Pi =  4 - 4/3 + 4/5 - 4/7 + 4/9 - 4/11...
    c) 1 - 1/2 + 1/4 - 1/6 + 1/8 - 1/10 + 1/12...
    Las iteraciones de cada serie deben parar cuando el último término sumado o restado sea 
    menor de 0.01
    */
    public void ejercicioNumeroSeis() {
        double acum, pi, acum2;
	int n, n2, n3, cont, cont2;
	
	acum = 0;
	n = 1;
	while (( 1.0 / n ) >= 0.01) {
            acum = acum + (1.0/n);
            //System.out.println(1.0/n);
            n++;
	}
        
        pi = 0;
	n2 = 1;
	cont = 1;
	while ((4.0/n2) >= 0.01) {
            if (cont % 2 == 0) {
		pi = pi - (4.0/n2);
            }
            else {
		pi = pi + (4.0/n2);	
            }
            
            //System.out.println(4.0/n2);
            n2 = n2 + 2;
            cont++;
	}
        
        acum2 = 1;
	n3 = 2;
	cont2 = 2;
	while ((1.0/n3) >= 0.01) {
            if (cont2 % 2 == 0) {
		acum2 = acum2 - (1.0/n3);
            }
            else {
		acum2 = acum2 + (1.0/n3);
            }
            
            //System.out.println(1.0/n3);
            n3 = n3 + 2;
            cont2++;
	}
        
        System.out.println("El resultado de la serie a) es: " + acum);
	System.out.println("El resultado de la serie b) es: " + pi);
	System.out.println("El resultado de la serie c) es: " + acum2);
    }
    
    /*
    Método para sumar los números pares y multiplicar los números impares hasta que la suma sea 
    mayor que 50 y el producto mayor que 150.
    */
    public void ejercicioNumeroSiete() {
        int num, suma, prod;
        Scanner keyboard = new Scanner(System.in);
        
	suma = 0;
	prod = 1;
        /*
        El programa se mantiene al interior del ciclo while hasta que se deje de cumplir las dos
        condiciones que se están evaluando, es decir, se permite que se cumpla una de las dos
        condiciones y también se permite que se cumpla la condición restante; por eso se usa el 
        operador lógico "o" (||) 
        (0 || 0 = 0 --> Condición de paro del ciclo)
        */
	while(suma <= 50 || prod <= 150) {
            System.out.print("Dame un numero entero: ");
            num = keyboard.nextInt();
            
            if(num % 2 == 0) {
		suma = suma + num;
            }
            else {
                prod = prod * num;
            }
	}
        
	System.out.println("La suma es: " + suma );
	System.out.println("El producto es: " + prod );
    }
    
    /*
    Método para sumar los números pares y multiplicar los números impares hasta que la suma sea 
    mayor que 50 o el producto mayor que 150.
    */
    public void ejercicioNumeroOcho() {
        int num, suma, prod;
        Scanner keyboard = new Scanner(System.in);
        
	suma = 0;
	prod = 1;
        /*
        El programa se mantiene al interior del ciclo while hasta que se deje de cumplir alguna 
        de las dos condiciones que se están evaluando, con una que se deje de cumplir se termina el
        ciclo; por eso se usa el operador lógico "y" (&&) 
        (1 && 0 = 0 además, 0 && 1 = 0 --> Condiciones de paro del ciclo)
        */
	while(suma <= 50 && prod <= 150) {
            System.out.print("Dame un numero entero: ");
            num = keyboard.nextInt();
            
            if(num % 2 == 0) {
		suma = suma + num;
            }
            else {
                prod = prod * num;
            }
	}
        
	System.out.println("La suma es: " + suma );
	System.out.println("El producto es: " + prod );
    }
    
    /*
    Métodod para calcular el factorial de un número entero
    */
    public void ejercicioNumeroNueve() {
        int num, num2;
        long fac; // Podemos usar el tipo long para enteros muy grandes
        Scanner keyboard = new Scanner(System.in);
	System.out.print("Escribe un número: ");
	num = keyboard.nextInt();
        
	num2 = num;
	fac = 1;
	while(num != 1) {
            fac = fac * num;
            num--;
	}
        
        System.out.println("El factorial de " + num2 +" es: " + fac);
    }
    
    /*
    Método para elevar un número real a una potencia entera e imprimir el resultado
    correspondiente.
    */
    public void ejercicioNumeroDiez() {
        int pot, cont;
	double num, res;
        Scanner keyboard = new Scanner(System.in);
        
	System.out.print("Escribe un numero real: ");
	num = keyboard.nextDouble();
	System.out.print( "Escribe la potencia: " );
	pot = keyboard.nextInt();
	
        res = 1;
	cont = 0;
	while (cont != pot) {
            res = res * num;
            cont++;
	}
	System.out.println( "El resultado es: " + res);
    }
   
    /*
    Mẃtodso para calcular la media de un conjunto de n número reales.
    */
    public void ejercicioNumeroOnce() {
        int n, cont;
	double media, num, acum;
        Scanner keyboard = new Scanner(System.in);
        
	System.out.print( "Dame la cantidad de numeros: ");
	n = keyboard.nextInt();
	
        cont = 0;
	acum = 0;
	while (cont != n) {
            System.out.print("Dame un numero real: ");
            num = keyboard.nextDouble();
            acum = acum + num;
            cont++;
	}
        
	media = acum / n;
	System.out.println("La media es: " + media);
    }
    
    /*
    Método para imprimir de forma inversa los números del 100 al 0, con decremento de dos
    */
    public void ejercicioNumeroDoce() {
        int n;
	n = 100;
	while(n >= 0) {
            System.out.println(n);
            n = n - 2;
	}
    }
    
    /*
    Método para leer un número entero e imprimir si éste es número primo o no
    */
    public void ejercicioNumeroTrece() {
        int num, cont, cont2;
        Scanner keyboard = new Scanner(System.in);
        
	System.out.print("Dame un numero: ");
	num = keyboard.nextInt();
	
        cont = 0; // variable para contar el numero de multiplos de un numero
	cont2 = 1; // variable para controlar el ciclo e ir dividiendo el número entre ese contador y evaluar si su valor es múltiplo del número
	while(cont2 <= num) // Desde 1 hasta num
	{
            // el ciclo evalua desde 1 hasta el numero ingresado si hay multiplos del mismo y los va contando, si hay solo 2 multiplos el
            // número es primo (el 1 y el mismo número)
            if ( num % cont2 == 0 ) {
		cont++;
            }
			
            cont2++;
	}
        
	if(cont == 2) {
            System.out.println(num + " es numero primo.");
        }
        else {
            System.out.println(num + " no es numero primo.");
        }
    }
}
